/**
 * @author Joseph Gonzales
 */
package com.revature.stringcount;

public class StringCounter {

	public static void main(String[] args) {
		//check incase there is nothing in the arguments
		try {
			//length expresses the length of the string of the first argument
			//all other arguments are not required
			System.out.println(args[0].length());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.err.println("No arguments specified!");
		}
	}

}
