/**
 * @author Joseph Gonzales
 */
package com.revature.modulate;

import java.util.Scanner;

public class EvenDetector {

	public static void main(String[] args) {
		int num = 0;
		if(args.length >= 1) {
			//throw incase of input errors
			try {
				num = Integer.getInteger(args[0]);
			}catch(Exception e) {
				System.err.println("Invalid input");
				return;
			}
		}
		else {
			//create scanner to take in input
			Scanner rd = new Scanner(System.in);
			try {
				num = rd.nextInt();
			}
			//catch invalid inputs
			catch(Exception e) {
				System.err.println("Invalid input");
			}
			//close up the resource just in case
			finally {rd.close();}
		}
		//diving an integer by half results as an integer, since a float is outputted then truncated to an int
		//if there was a remainder, the truncation removes it, resulting in a smaller number
		//the smaller number is multiplied back by double, which should result in the same or smaller number
		//this indicates if the number was odd from lost data in the division
		if((num/2)*2 == num) System.out.print("Integer "+ num +" is even");
		else System.out.print("Integer "+ num +" is odd");
	}

}
