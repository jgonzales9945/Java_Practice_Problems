/**
 * @author Joseph Gonzales
 */
package com.revature.palindrome;

import java.util.*;

public class ArrayPalindrome {
	
	public static boolean isPalindrome(String value) {
	    if (value == null || value.isEmpty()) return false;
	    //check for palindrome, by reversing the string and checking if it is equals to the original string
	    return new StringBuffer(value).reverse().toString().equals(value);
	}
	
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>(Arrays.asList("karan", "madam", "tom", "civic", "radar", "jimmy", "kayak", "john",  "refer", "billy", "did"));
		ArrayList<String> palindromes = new ArrayList<>();
		//find palindromes out of the list, then add them to the palindromes list
		for(String i : list) if(isPalindrome(i)) palindromes.add(i);
		//output all palindromes
		for(String j : palindromes) System.out.println(j);
	}

}
