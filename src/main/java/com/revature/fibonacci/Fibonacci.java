/**
 * @author Joseph Gonzales
 */
package com.revature.fibonacci;

public class Fibonacci {
	
	//recursively add numbers to gether to produce fibonacci numbers
	public static int fib(int n) {
		if(n <= 1) return n;// 0 and 1 would only add to each other in the first 3 iterations
		else return fib(n-1) + fib(n-2);// add the last two numbers to gether
	}
	
	public static void main(String[] args) {
		//iterate every fibonacci number until 25
		for (int i = 0; i <= 25; i++) System.out.println(fib(i));
	}

}
