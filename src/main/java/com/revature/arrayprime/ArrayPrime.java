/**
 * @author Joseph Gonzales
 */
package com.revature.arrayprime;

import java.util.ArrayList;

public class ArrayPrime {

	public static void main(String[] args) {
		ArrayList<Integer> primeList = new ArrayList<>();
		//generate list of numbers from 0 to 100
		for(int i = 0; i <= 100; i++) primeList.add(i);
		//iterate through the list
		for (int n = 1; n <= primeList.size(); n++) {
			//get rid of 1
			if(n == 1) primeList.set(n, 0);
			//iterate through odd numbers after 2 to find primes 
            for (int j = 2; j < n; j++) {
                if (n % j == 0) {
                	//set the value of the composite number to 0 for removal
                    primeList.set(n, 0);
                    break;
                }
            }
        }
		for (int k : primeList) {
			//print out all the prime numbers that haven't been labeled 0
			//all zeros are skipped
            System.out.print(k != 0 ? k +"\n":"");
        }
	}

}
