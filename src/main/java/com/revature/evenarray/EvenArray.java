/**
 * @author Joseph Gonzales
 */
package com.revature.evenarray;

import java.util.ArrayList;

public class EvenArray {

	public static void main(String[] args) {
		ArrayList<Integer> evenList = new ArrayList<>();
		//generate list of numbers from 0 to 100
		for(int i = 0; i <= 100; i++) evenList.add(i);
		//iterate through the arraylist, then output even numbers if the modulation results in 0
		for(int j : evenList) System.out.print( (j%2)==0 ? j +"\n" : "");
	}

}
