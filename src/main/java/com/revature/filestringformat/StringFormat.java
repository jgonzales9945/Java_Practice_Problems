package com.revature.filestringformat;

import java.io.*;

public class StringFormat {

	public static void main(String[] args) {
		String path = "resource/Data.txt";
		try {
			BufferedReader fread = new BufferedReader(new FileReader(path));
			String line = fread.readLine();
			while(line != null) {
				String[] arr = line.split(":");
				System.out.println("Name: "+ arr[0] +" "+ arr[1]);
				System.out.println("Age: "+ arr[2] +" years");
				System.out.println("State: "+ arr[3] +" state");
				line = fread.readLine();
			}
			fread.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
