/**
 * @author Joseph Gonzales
 */
package com.revature.factorial;

import java.util.*;

public class Factorial {

	public static void main(String[] args) {
		int n = 0;
		if(args.length >= 1) {
			//make sure the argument is a integer, throw otherwise
			try {
				n = Integer.getInteger(args[0]);
			}catch(Exception e) {
				System.err.println("Invalid input");
				return;
			}
		}
		else {
			//create scanner to take in input
			Scanner rd = new Scanner(System.in);
			try {
				n = rd.nextInt();
			}
			//catch invalid inputs such as strings or other types
			catch(Exception e) {
				System.err.println("Invalid input");
			}
			//close up the resource just in case
			finally {rd.close();}
		}
		System.out.println(fac(n));
	}

	private static int fac(int n) {
		if(n <= 1) return n;
		else return n * fac(n-1);
	}

}
