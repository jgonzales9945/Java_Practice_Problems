/**
 * @author Joseph Gonzales
 */
package com.revature.bubblesort;

public class BubbleSort {

	//Iterate through an array, swapping numbers that are larger than the next iteration
	//Make sure each loop iterated to the end of the loop to prevent going out of bounds
	//O^2 complexity due to the two for loops.
	public static int[] bubbleSort(int[] input){
		int temp = 0;
		for(int count = 0; count < input.length-1; count++) {
			for(int i = 0; i < input.length-1; i++) {
				if(input[i] > input[i+1]){
					temp = input[i];
					input[i] = input[i+1];
					input[i+1] = temp;
				}
			}
		}
		return input;
	}
	public static void main(String[] args) {
		int[] array = {1,0,5,6,3,2,3,7,9,8,4};
		
		int[] sorted = bubbleSort(array);
		
		for(int i : sorted) {
			System.out.println(i);
		}
	}

}
