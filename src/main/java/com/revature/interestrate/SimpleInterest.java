/**
 * @author Joseph Gonzales
 */
package com.revature.interestrate;

import java.util.Scanner;

public class SimpleInterest {

	public static void main(String[] args) {
        float principle, rate, time;
        Scanner scan = new Scanner(System.in);
        //get the principle
        System.out.print("Enter the principal: ");
        principle = scan.nextFloat();
        //get the interest rate
        System.out.print("\nEnter the interest rate: ");
        rate = scan.nextFloat();
        //get the time in years
        System.out.print("\nEnter the time span in years: ");
        time = scan.nextFloat();
        scan.close();//close resource
        //output the simple interest I=Prt
        System.out.println("\nSimple Interest rate:"+ (rate * time * principle));
	}

}
