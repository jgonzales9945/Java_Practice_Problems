package com.revature.ternaryminimum;

import java.util.Scanner;

public class TernMinimum {

	public static void main(String[] args) {
		int a = 0, b = 1;
		if(args.length >= 1) {
			//throw incase of input errors
			try {
				a = Integer.getInteger(args[0]);
				b = Integer.getInteger(args[1]);
			}catch(Exception e) {
				System.err.println("Invalid input");
				return;
			}
		}
		else {
			//create scanner to take in input
			Scanner rd = new Scanner(System.in);
			try {
				System.out.print("Enter the first number: ");
				a = rd.nextInt();
				System.out.print("\nEnter the second number: ");
				b = rd.nextInt();
			}
			//catch invalid inputs
			catch(Exception e) {
				System.err.println("Invalid input");
			}
			//close up the resource just in case
			finally {rd.close();}
		}
		//evaluation happens in the println, checking for a minimum or for equivalency with ternary
		System.out.println(a < b ? a+" is the minimum" : b == a ? "both numbers are minimum" : b+" is the minimum");
	}

}
