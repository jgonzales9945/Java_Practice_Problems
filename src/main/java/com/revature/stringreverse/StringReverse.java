/**
 * @author Joseph Gonzales
 */
package com.revature.stringreverse;

import java.util.Scanner;

public class StringReverse {

	public static void main(String[] args) {
		String norm = "Default String";
		if(args.length >= 1) {
			//throw incase of input errors
			try {
				norm = args[0];
			}catch(Exception e) {
				System.err.println("Invalid input");
				return;
			}
		}
		else {
			//create scanner to take in input
			Scanner rd = new Scanner(System.in);
			try {
				norm = rd.next();
			}
			//catch invalid inputs
			catch(Exception e) {
				System.err.println("Invalid input");
			}
			//close up the resource just in case
			finally {rd.close();}
		}
		//append the last to first characters of the string to itself
		//decrement the index to find the character
		for(int i = norm.length()-1; i >= 0; i--) {
			norm+=norm.charAt(i);
		}
		//the result should be the string itself with the reverse string appended
		//split it at half of the length and save the new result
		norm = norm.substring(norm.length()/2);
		System.out.println(norm);
	}

}
