/**
 * @author Joseph Gonzales
 */
package com.revature.superstrings;

public class StringRunner {

	public static void main(String[] args) {
		StringHandler strg = new StringHandler();
		String myString = "4 what will HAPPEN to this String?";
		String intString = "234";
		
		if(strg.upperCaseCheck(myString) == true) System.out.println("The string has Capital Letter(s)");
		else  System.out.println("The string has no Capital Letter");
		
		System.out.println(strg.lowerCaseConvert(myString));
		
		System.out.println(strg.stringIntConvert(myString));
		System.out.println(strg.stringIntConvert(intString));
	}

}

class StringHandler extends ParentString {

	@Override
	public boolean upperCaseCheck(String i) {
		//go through every character in the string, if there is a capital letter, immediately return true
		for(int u =0; u < i.length(); u++) if(Character.isUpperCase(i.charAt(u)) == true) return true;
		//no capital letters found
		return false;
	}

	@Override
	public String lowerCaseConvert(String j) {
		//convert string to uppercase
		return j.toUpperCase();
	}

	@Override
	public int stringIntConvert(String k) {
		//get the value of the string anf add 10 to it
		return Integer.valueOf(k.length()) + 10;
	}
	
}
