/**
 * @author Joseph Gonzales
 */
package com.revature.superstrings;

public abstract class ParentString {
	public abstract boolean upperCaseCheck(String i);
	public abstract String lowerCaseConvert(String j);
	public abstract int stringIntConvert(String k);
}
