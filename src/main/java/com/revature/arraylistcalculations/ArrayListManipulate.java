/**
 * @author Joseph Gonzales
 */
package com.revature.arraylistcalculations;

import java.util.ArrayList;

public class ArrayListManipulate {

	public static void main(String[] args) {
		ArrayList<Integer> cList = new ArrayList<>();
		int evens = 0, odds = 0;
		//generate list of numbers from 0 to 100
		for(int i = 1; i <= 10; i++) cList.add(i);
		//iterate through the list
		for(int j = 0; j < cList.size(); j++) System.out.print(cList.get(j) +" ");
		
		for(int n = 0; n < cList.size(); n++) {
			if(cList.get(n) %2 == 0) evens += cList.get(n);
			else odds += cList.get(n);
		}
		//remove prime numbers from the array
		for (int n = 0; n < cList.size(); n++) {
			//check for composites, default to true assuming if the number is prime
			boolean ch = true;
			if(cList.get(n) == 1) cList.set(n, 0);
			
			//iterate through all numbers to find composite numbers
			//composites tend to be even or a multiple of an odd number
            for (int j = 2; j < cList.get(n); j++) {
            	//prime occurs when the value of j is equal to n
            	//since j is in theory supposed to be a multiple of the number at the cList index specified by n
                if (cList.get(n) % j == 0) {
                	ch = false;
                	break;
                }
            }
            //mark primes with 0, to be removed later
            if(ch) cList.set(n, 0);
        }
		System.out.print("\nEven total: "+ evens);
		System.out.print("\nOdd total: "+ odds +"\n");
		for (int k : cList) {
			//print out all the composite numbers
			System.out.print(k != 0 ? k +" ":"");
        }
	}

}
