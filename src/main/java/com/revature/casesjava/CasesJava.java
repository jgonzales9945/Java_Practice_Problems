/**
 * @author Joseph Gonzales
 */
package com.revature.casesjava;

import java.util.Date;

public class CasesJava {

	public static void main(String[] args) {
		//print out usage statement on null arguments, skip if 2 is in the first argument
		if(args.length < 2 && !args[0].equalsIgnoreCase("2")) {
			System.out.println("Please enter 1. for square root option and another number to get the root of\n"+
								"2. to get the current time and date\n"+
								"3. to split a string by it's spaces using the second argument\n"+
								"Any other input will show the proper usage.");
			return;
		}
		switch(args[0]) {
		case "1"://go to square root
			sqRoot(args[1]);
			break;
		case "2"://print out the date
			getDate();
			break;
		case "3"://split the string up by spaces
			if(!args[1].contains(" ")) {
				System.out.println("No spaces in the string available to split!");
				return;
			}
			splitString(args[1]);
			break;
		default:
			System.out.println("USAGE: [argument 1-3] [1. number to square root]|[3. string to split]");
			return;
		}
	}

	private static void splitString(String args) {
		//iterate through the string, outputing all substrings delimeted by a space
		for (String extract : args.split(" ")) {
	         System.out.println(extract);
	      }
		
	}

	private static void getDate() {
		//print date and time
		System.out.println(new Date().toString());
		
	}

	private static void sqRoot(String args) throws IllegalArgumentException {
		//parse the string to a double
		double p;
		try {
			//Number format exception should be thrown if the argument does not contain a double
			p = Double.parseDouble(args);
			//output the squareroot of the resulting double
			System.out.println("The Squareroot of "+p+": "+ Math.sqrt(p));
        } catch (NumberFormatException e) {
        	System.err.println("String does not contain any double values");
        }
		return;
	}
}