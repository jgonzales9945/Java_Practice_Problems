/**
 * @author Joseph Gonzales
 */
package com.revature.binarytriangle;

public class BinaryTriangle {

	public static void main(String[] args) {
		for(int i = 0; i <= 5; i++) {
			for(int j = 1; j <= i; j++) {
				//add the numbers together, modulate to set the number according to its location on the triangle
				System.out.print((i + j) % 2+" ");
			}
			System.out.print("\n");
		}
	}

}