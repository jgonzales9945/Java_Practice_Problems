/**
 * @author Joseph Gonzales
 */
package com.revature.substring;

import java.util.Scanner;

public class SubString {

	public static void main(String[] args) {
		String orig = "Original String";
		int indx = 0;
		if(args.length >= 2) {
			//make sure the argument is a integer, throw otherwise
			try {
				orig = args[0];
				indx = Integer.getInteger(args[1]);
			}catch(Exception e) {
				System.err.println("Invalid input");
				return;
			}
		}
		else {
			//create scanner to take in input
			Scanner rd = new Scanner(System.in);
			try {
				//get string input
				System.out.print("Input a String: ");
				orig = rd.nextLine();
				//get integer for index number
				System.out.print("\nInput a index number: ");
				indx = rd.nextInt();
			}
			//catch invalid inputs such as strings or other types
			catch(Exception e) {
				System.err.println("Invalid input");
			}
			//close up the resource just in case
			finally {rd.close();}
		}
		System.out.println(subString(orig, indx));
	}

	private static String subString(String orig, int indx) {
		if(indx > orig.length()) return "String Index out of Bounds";
		else {
			String temp = "";
			//loop through every subindex between 0 and the specified index
			//take the character out of the string at the subindex and append it to the temp string
			for(int i = 0; i <= indx; i++) {
				temp += orig.charAt(i);
			}
			return temp;
		}
	}

}
