/**
 * @author Joseph Gonzales
 */
package com.revature.floats;

import com.revature.floats.fieldfloats.Floats;

public class FloatAccess {

	public static void main(String[] args) {
		Floats nFl = new Floats();
		System.out.println("First float: "+ nFl.getF1() +"\nSecond float: "+ nFl.getF2());

	}

}
