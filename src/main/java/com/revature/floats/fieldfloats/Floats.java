/**
 * @author Joseph Gonzales
 */
package com.revature.floats.fieldfloats;

public class Floats {
	
	private float f1;
	private float f2;
	
	public Floats() {
		this.f1=-1232;
		this.f2=327;
	}

	public float getF1() {
		return f1;
	}

	public void setF1(float f1) {
		this.f1 = f1;
	}

	public float getF2() {
		return f2;
	}

	public void setF2(float f2) {
		this.f2 = f2;
	}

}
