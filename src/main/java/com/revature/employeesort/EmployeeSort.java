/**
 * @author Joseph Gonzales
 */
package com.revature.employeesort;

import java.util.*;

public class EmployeeSort {

	public static void main(String[] args) {
		List<Employee> employees = new ArrayList<Employee>();
        employees.add(new Employee("Chandler", "IT", 28));
        employees.add(new Employee("Timmothy", "Sys Admin", 23));
		Collections.sort(employees, new Employee());
		
		for(Employee em : employees) {
			System.out.println(em.toString());
		}
	}

}


class Employee implements Comparator<Employee> {
	
	public String name;
	public String department;
	public int age;
	
	public Employee(String name, String department, int age) {
		this.name = name;
		this.department = department;
		this.age = age;
	}
	
	public Employee() {
		
	}
	@Override
	public int compare(Employee o1, Employee o2) {
		//compares strings of the employee properties to see if they are greater, equal, or less than eachother
		int rName = o1.name.toLowerCase().compareTo(o2.name);
		int rDep = o1.department.toLowerCase().compareTo(o2.department);
		int rAge = o1.age < o2.age ? -1 : o1.age == o2.age ? 0 : 1;
		//returns result of the greater variable
		if(rName > 0 || rDep > 0 || rAge > 0) return 1;
		//this is not ideal
		else if(rName == 0 || rDep == 0 || rAge == 0) return 0;
		//this might night even happen if a greater value is detected, need more evaluation somewhere
		else return -1;
	}
	@Override
    public String toString() {
        return String.format("name="+name+", department="+department+", age="+age);
    }
}
