/**
 * @author Joseph Gonzales
 */
package com.revature.interfacingclass;

public class Mathematics implements BasicMath {
	//adds two numbers together
	@Override
	public float add(float x, float y) {
		return x+y;
	}
	//subtracts two numbers from eachother
	@Override
	public float subtract(float x, float y) {
		return x-y;
	}
	//multiplies two numbers together
	@Override
	public float multiply(float x, float y) {
		return x*y;
	}
	//divides two numbers
	@Override
	public float divide(float x, float y) {
		if(y == 0) System.out.println("Division by Zero detected!");
		return x/y;
	}

	public static void main(String[] args) {
		Mathematics mathematics = new Mathematics();
		//tests the mathematics methods
		System.out.println(mathematics.add(4, mathematics.subtract(9, 3)));
		System.out.println(mathematics.divide(8, mathematics.multiply(4, 12)));
		System.out.println(mathematics.divide(5, 0));

	}

}
