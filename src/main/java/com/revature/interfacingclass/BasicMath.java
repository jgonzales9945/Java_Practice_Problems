/**
 * @author Joseph Gonzales
 */
package com.revature.interfacingclass;
//defines methods for basic mathematical computation
public interface BasicMath {
	float add(float x, float y);
	float subtract(float x, float y);
	float multiply(float x, float y);
	float divide(float x, float y);
}
